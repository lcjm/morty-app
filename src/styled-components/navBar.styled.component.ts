import styled from "styled-components";

export const NavBarContainer = styled.header`
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 20px;
  padding-bottom: 20px;
  display: flex;
`;
