import styled from "styled-components";
export const ContainerApp = styled.main`
  width: 100%;
  min-height: 100vh;
  padding: 0;
  background-color: #0d0d0d;
  margin: 0;
`;
