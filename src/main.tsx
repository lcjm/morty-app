import { axiosInterceptor } from "./interceptors";
import {
  Route,
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
} from "react-router-dom";
import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { Layout } from "./components/layout";
import Home from "./pages/home/Home";

axiosInterceptor();

const router = createBrowserRouter(
  createRoutesFromElements(<Route path="/" element={<Home />} />)
);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <Layout>
      <RouterProvider router={router} />
    </Layout>
  </React.StrictMode>
);
