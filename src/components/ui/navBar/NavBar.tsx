import React from "react";
import { NavBarContainer } from "../../../styled-components/navBar.styled.component";
export interface NavBarInterface {}

const NavBar: React.FC<NavBarInterface> = () => {
  return (
    <NavBarContainer>
      <img src="public/assets/icn-header-rick.png" width={45} />
      <p> Rick And Morty App </p>
    </NavBarContainer>
  );
};

export default NavBar;
