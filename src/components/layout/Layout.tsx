import React from "react";
import { ContainerApp } from "../../styled-components/container.styled.component";
import { NavBar } from "../ui/navBar";
export interface LayoutInterface {}

const Layout: React.FC<LayoutInterface> = () => {
  return (
    <ContainerApp>
      <NavBar />
    </ContainerApp>
  );
};

export default Layout;
