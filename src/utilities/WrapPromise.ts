import { TypeWithKey } from "../models/index";

const wrapPromise = (promise: any) => {
  let status = "pending";
  let result: any;

  const suspender = promise().then(
    (res: any) => {
      status = "success";
      result = res;
    },
    (err: any) => {
      status = "error";
      result = err;
    }
  );

  const handler: TypeWithKey<any> = {
    pending: () => {
      throw suspender;
    },
    error: () => {
      throw result;
    },
    default: () => result,
  };

  const read = () => {
    const result = handler[status] ? handler[status]() : handler.default();
    return result;
  };

  return { read };
};

export default wrapPromise;
