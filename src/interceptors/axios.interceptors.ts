import axios from "axios";

export const axiosInterceptor = () => {
  axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      return error;
    }
  );
};
